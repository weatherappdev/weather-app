import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CityDetailsContainerComponent } from './city-details-container.component';

describe('CityDetailsContainerComponent', () => {
  let component: CityDetailsContainerComponent;
  let fixture: ComponentFixture<CityDetailsContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CityDetailsContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CityDetailsContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
