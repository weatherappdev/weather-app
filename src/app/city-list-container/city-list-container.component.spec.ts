import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CityListContainerComponent } from './city-list-container.component';

describe('CityListContainerComponent', () => {
  let component: CityListContainerComponent;
  let fixture: ComponentFixture<CityListContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CityListContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CityListContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
