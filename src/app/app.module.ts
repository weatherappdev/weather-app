import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { SearchCityComponent } from './search-city/search-city.component';
import { CityDetailsContainerComponent } from './city-details-container/city-details-container.component';
import { CityDetailsComponent } from './city-details-container/city-details/city-details.component';
import { DateComponent } from './date/date.component';
import { CityListContainerComponent } from './city-list-container/city-list-container.component';
import { CityListComponent } from './city-list-container/city-list/city-list.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    SearchCityComponent,
    CityDetailsContainerComponent,
    CityDetailsComponent,
    DateComponent,
    CityListContainerComponent,
    CityListComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,MDBBootstrapModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
